package com.example.bai1.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.bai1.model.Book

@Dao
interface BookDao {
    @Query("SELECT * FROM Book")
    fun getAll(): List<Book>

    @Insert
    fun insertAll(books: Book)

    @Delete
    fun delete(book: Book)

    @Query("SELECT * FROM BOOK ORDER BY id ASC")
    fun readAllBook():LiveData<List<Book>>
}