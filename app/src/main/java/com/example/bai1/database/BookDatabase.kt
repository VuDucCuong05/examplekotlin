package com.example.bai1.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.bai1.model.Book

@Database(entities = [Book::class], version = 1, exportSchema = false)
abstract class BookDatabase: RoomDatabase() {
}