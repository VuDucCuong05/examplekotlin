package com.example.bai1.fragment.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.bai1.fragment.home.tablayout_home.EconomyFragment
import com.example.bai1.fragment.home.tablayout_home.EducationFragment
import com.example.bai1.fragment.home.tablayout_home.Sport

class AdapTabLayout(fm: FragmentManager, val fragmentCount: Int) : FragmentStatePagerAdapter(fm) {

    private val fragmentTitleList =
        mutableListOf("Economy", "Education", "Sport")

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> EconomyFragment()
            1 -> EducationFragment()
            2 -> Sport()
            else -> return EconomyFragment()

        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitleList[position].toString()
    }

    override fun getCount(): Int = fragmentCount
}
