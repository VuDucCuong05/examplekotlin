package com.example.bai1.fragment.download

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bai1.R
import com.example.bai1.model.Book
import java.util.*

class AdapterBookDownload(
    private val mList: List<Book>
) : RecyclerView.Adapter<AdapterBookDownload.ViewHolder>() {
    private var clickListener: ((Book) -> Unit)? = null

    fun setClickListener(listener: ((Book) -> Unit)) {
        clickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_book_download, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book = mList[position]
        holder.itemImage.setImageResource(book.image)
        holder.itemName.text = book.name
        holder.layoutBook.setOnClickListener {
            clickListener?.let {
                it(book)
            }
        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemImage: ImageView
        var itemName: TextView
        var layoutBook: LinearLayout

        init {
            itemImage = itemView.findViewById(R.id.br_book)
            itemName = itemView.findViewById(R.id.text_name_book)
            layoutBook = itemView.findViewById(R.id.layout_book)
        }

    }


}