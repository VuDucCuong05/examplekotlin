package com.example.bai1.fragment.download

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bai1.R
import com.example.bai1.fragment.file.AdapterBook
import com.example.bai1.model.Book

class DownloadFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_download, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rc_book_download)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val dataBook = ArrayList<Book>()
        for (i in 1..20) {
            dataBook.add(Book(i, "Book " + i, R.drawable.book1, "Book content" + i))
        }
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(
            activity!!.baseContext,
            2, RecyclerView.VERTICAL, false
        )
        val adapterBook = AdapterBook(dataBook,1)
        recyclerView.adapter = adapterBook
        recyclerView.layoutManager = layoutManager


        return view
    }

}