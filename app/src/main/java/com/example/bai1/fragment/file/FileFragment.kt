package com.example.bai1.fragment.file

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bai1.model.Book
import com.example.bai1.R

class FileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_file, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rc_book)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val dataBook = ArrayList<Book>()
        for (i in 1..20) {
            dataBook.add(Book(i, "Book " + i, R.drawable.ic_book, "Book content" + i))
        }
        val adapterBook = AdapterBook(dataBook,0)
        recyclerView.adapter = adapterBook
        adapterBook.setClickListener {
            val action = FileFragmentDirections.actionFileFragmentToReadBookFragment(it)
            requireActivity().findNavController(R.id.nav_host_fragment).navigate(action)
        }

        val searchBook = view.findViewById<SearchView>(R.id.search_book)
        searchBook.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapterBook.getFilter().filter(newText)
                return false
            }

        })
        return view
    }

}