package com.example.bai1.fragment.file

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.bai1.R


class ReadBookFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_read_book, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: ReadBookFragmentArgs by navArgs()
        val book = args.book
        Log.e("prox", "" + book.toString())
        val textId = view.findViewById<TextView>(R.id.text_id)
        val textName = view.findViewById<TextView>(R.id.text_name)

        textId.text = book.id.toString()
        textName.text = book.name

        val imgArrowLeft = view.findViewById<ImageView>(R.id.img_arrow_left)
        imgArrowLeft.setOnClickListener {
            activity?.onBackPressed()
//            findNavController(view).navigate(R.id.action_readBookFragment_to_fileFragment)
        }
    }
}