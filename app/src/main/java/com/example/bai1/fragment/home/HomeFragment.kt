package com.example.bai1.fragment.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.bai1.R
import com.google.android.material.tabs.TabLayout

class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        val tabLayout = view.findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = view.findViewById<ViewPager>(R.id.viewPager)

        tabLayout.getTabAt(0)?.setText(R.string.economy)
        tabLayout.getTabAt(1)?.setText(R.string.education)
        tabLayout.getTabAt(2)?.setText(R.string.entertainment)
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        viewPager.adapter = AdapTabLayout(childFragmentManager, 3)

        viewPager.offscreenPageLimit = 3
        tabLayout.setupWithViewPager(viewPager)
        return view
    }

}