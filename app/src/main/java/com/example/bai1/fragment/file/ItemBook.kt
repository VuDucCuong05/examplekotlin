package com.example.bai1.fragment.file

import com.example.bai1.model.Book

interface ItemBook {
    fun onClickName(book: Book)
}