package com.example.bai1.fragment.file

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bai1.R
import com.example.bai1.model.Book
import java.util.*

class AdapterBook(
    private val mList: List<Book>, mViewType: Int
) : RecyclerView.Adapter<AdapterBook.ViewHolder>() {
    private var clickListener: ((Book) -> Unit)? = null

    fun setClickListener(listener: ((Book) -> Unit)) {
        clickListener = listener
    }

    // search
    var countryFilterList = ArrayList<String>()
    val strName = ArrayList<String>()

    //
    var viewType1 = 0

    init {
        for (i in 0..(mList.size - 1)) {
            strName.add(mList[i].name)
        }
        countryFilterList = strName
        //
        viewType1 = mViewType

    }

    //
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        when (viewType1) {
            0 -> {
                val v =
                    LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
                return ViewHolder(v)

            }
            1 -> {
                val v =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_book_download, parent, false)
                return ViewHolder(v)
            }
            else -> {
                val v =
                    LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
                return ViewHolder(v)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book = mList[position]
        holder.itemImage.setImageResource(book.image)
        holder.itemName.text = book.name
//        holder.layoutBook.setOnClickListener {
//            clickListener?.let {
//                it(book)
//            }
//        }
        // search
        val selectCountryTextView =
            holder.itemName.findViewById<TextView>(R.id.text_name_book)
        selectCountryTextView.text = countryFilterList[position]

        holder.layoutBook.setOnClickListener {
            Log.d("Selected:", countryFilterList[position])
            for (i in 0..mList.size - 1) {
                if (mList[i].name.equals(countryFilterList[position])) {
                    val book = mList[i]
                    clickListener?.let {
                        it(book)
                    }
                }
            }

        }
    }

    override fun getItemCount(): Int {
        return countryFilterList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemImage: ImageView
        var itemName: TextView
        var layoutBook: LinearLayout

        init {
            itemImage = itemView.findViewById(R.id.image_book)
            itemName = itemView.findViewById(R.id.text_name_book)
            layoutBook = itemView.findViewById(R.id.layout_book)
        }

    }

    // search
    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    countryFilterList = strName
                } else {
                    val resultList = ArrayList<String>()
                    for (row in strName) {
                        if (row.lowercase(Locale.ROOT)
                                .contains(charSearch.lowercase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    countryFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = countryFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                countryFilterList = results?.values as ArrayList<String>
                notifyDataSetChanged()
            }
        }
    }
}