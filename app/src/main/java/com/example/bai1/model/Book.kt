package com.example.bai1.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Book")
data class Book(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    @ColumnInfo(name = "first_name") val name: String,
    @ColumnInfo(name = "first_name") val image: Int,
    @ColumnInfo(name = "first_name") val content: String
): Serializable {

}

